// https://www.filamento.com/public/Product/Label/Data.xlsx
const raw= fetch('https://www.filamento.com/public/Product/Label/Data.xlsx').then(function (res) {
    /* get the data as a Blob */
    if (!res.ok) throw new Error("fetch failed");
    return res.arrayBuffer();
})
.then(function (ab) {
    /* parse the data when it is received */
    var data = new Uint8Array(ab);
    var workbook = XLSX.read(data, {
        type: "array"
    });

    var Raw= workbook.SheetNames[0];
    /* Get worksheet */
    var worksheet_0 = workbook.Sheets[Raw];
    
    let Raw_data= XLSX.utils.sheet_to_json(worksheet_0, { raw: true });
    console.log(Raw_data);
    return Raw_data;
});

const lumen= fetch('https://www.filamento.com/public/Product/Label/Data.xlsx').then(function (res) {
        /* get the data as a Blob */
        if (!res.ok) throw new Error("fetch failed");
        return res.arrayBuffer();
    })
    .then(function (ab) {
        /* parse the data when it is received */
        var data = new Uint8Array(ab);
        var workbook = XLSX.read(data, {
            type: "array"
        });
    
        var Lumen= workbook.SheetNames[2];
        /* Get worksheet */
        var worksheet_2 = workbook.Sheets[Lumen];
        
        let Lumen_data= XLSX.utils.sheet_to_json(worksheet_2, { raw: true });
        console.log(lumen)
        return Lumen_data;
    });



const Info= fetch('https://www.filamento.com/public/Product/Label/Data.xlsx').then(function (res) {
        /* get the data as a Blob */
        if (!res.ok) throw new Error("fetch failed");
        return res.arrayBuffer();
    })
    .then(function (ab) {
        /* parse the data when it is received */
        var data = new Uint8Array(ab);
        var workbook = XLSX.read(data, {
            type: "array"
        });
    
        var In= workbook.SheetNames[1];
        /* Get worksheet */
        var worksheet_1 = workbook.Sheets[In];
        
        let Info_data= XLSX.utils.sheet_to_json(worksheet_1, { raw: true });
        console.log(Info_data)
        return Info_data
    });



//Filter Submit
const Submit = async () => {
        lu = await lumen;
        In = await Info;
        ra = await raw;

        document.getElementById("error").innerHTML = '';
        const fa= document.getElementById('Family').value
        const mo=document.getElementById('Mounting').value
        const po=document.getElementById('Power').value
        const cri=document.getElementById('CRI').value
        const cct=document.getElementById('CCT').value
        const dri=document.getElementById('Driver').value
        document.getElementById('Part').innerHTML=`${fa}-${mo}-${po}-${cri}-${cct}-${dri}-AN-B90-000-000-FL`
        document.getElementById('PartNumber_box').innerHTML=`<input class="input_text" id="PartNumber_Box_input" size=40 type="text" style="height: 15px" value="${fa}-${mo}-${po}-${cri}-${cct}-${dri}-AN-B90-000-000-FL"></input>`;
        document.getElementById('PartNumber_card').innerHTML=`<input class="input_text" id="PartNumber_input" type="text" size=30 style="height: 12px" value="${fa}-${mo}-${po}-${cri}-${cct}-${dri}-AN-B90-000-000-FL"></input>`;

       


        //Title & Product code
        const code = ra.filter((item)=>{
            const temp=document.getElementById('Part').innerHTML
            console.log(temp)
            return item&&item.PartNumber===temp
        })

        document.getElementById('Title').innerHTML=code[0]?`${code[0].Title.toUpperCase()}`:`<span class="error">N/A</span>`;
        document.getElementById('box_title').innerHTML=code[0]?`<input class="input_text" id="box_title_input" type="text" value="${code[0].Title.toUpperCase()}"></input>`:`<span class="error">N/A</span>`;
        document.getElementById('Title_card').innerHTML=code[0]?`<input class="input_text" id="Title_card_input" style="width: 100px; height: 12px; padding:0" type="text" value="${code[0].Title.toUpperCase()}"></input> <strong>(Modelo,  <span>모델, モデル</span>)</strong>`:`<span class="error">N/A</span>`

        

        const zeroPad = (num, places) => String(num).padStart(places, '0')

        document.getElementById('Code').innerHTML=code[0]?`${zeroPad(code[0].Code, 5)}`:`<span class="error">N/A</span>`
        document.getElementById('Code_box').innerHTML=code[0]?`<input class="input_text" id="Code_box_input" type="text" size=10 style="height: 15px" value="${zeroPad(code[0].Code, 5)}"></input>`:`<span class="error">N/A</span>`
        document.getElementById('PartNumber_card').innerHTML+=code[0]?`<input class="input_text" id="PartNumber_card_input" type="text" style="width: 32px; height: 12px; padding:0" value="${zeroPad(code[0].Code, 5)}"></input>`:``

        //Barcode
        console.log((code[0].Barcode))
        document.getElementById('footer').innerHTML=`<svg class="barcode" id='barcode'
        jsbarcode-format="upc"
        jsbarcode-value=${code[0].Barcode}
        jsbarcode-textmargin="0"
        jsbarcode-fontoptions="bold">
        </svg>`
        JsBarcode(".barcode").init();

        const e_value = In.filter((item)=>{
            return item&&item.Family===fa&&item.Mounting===mo&&item.Power===parseInt(po)&&item.Driver===dri

        })

        document.getElementById('Electrical').innerHTML= e_value[0]?`${e_value[0].Electrical_Input}`:`<span class="error">N/A</span>`
        document.getElementById('Electrical_box').innerHTML= e_value[0]?`<input class="input_text" id="Electrical_box_input" size=35 type="text" style="height: 15px; padding:0" value="${e_value[0].Electrical_Input}"></input>`:`<span class="error">N/A</span>`
        document.getElementById('Electrical_card').innerHTML= e_value[0]?`<input class="input_text" id="Electrical_card_input" type="text" style="width: 150px; height: 10px; padding:0" value="${e_value[0].Electrical_Input}"></input>`:`<span class="error">N/A</span>`
        
       //Optical Output
        const lu_value = lu.filter((item)=>{
            return item.Family===fa&&item.Power===parseInt(po)&&item.CCT===parseInt(cct)&&item.CRI.toString()===cri.toString()
        })
        document.getElementById('Optical').innerHTML=lu_value[0]? `${lu_value[0].Lumen} Lumens, xxx cd, CCT ${cct}00K, CRI>${cri}0`:`<span class="error">N/A Lumens, xxx cd, CCT ${cct}00K, CRI>${cri}0</span>`
        document.getElementById('Optical_box').innerHTML=lu_value[0]? `<input class="input_text" id="Optical_box_input" type="text" style="width: 180px; height: 12px; padding:0" value="${lu_value[0].Lumen} Lumens, xxx cd, CCT ${cct}00K, CRI>${cri}0"></input>`:`<span class="error">N/A Lumens, xxx cd, CCT ${cct}00K, CRI>${cri}0</span>`
        document.getElementById('Optical_card').innerHTML=lu_value[0]? `<input class="input_text" id="Optical_card_input" type="text" style="width: 160px; height: 12px; padding:0" value="${lu_value[0].Lumen} Lumens, xxx cd, CCT ${cct}00K, CRI>${cri}0"></input>`:`<span class="error">N/A Lumens, xxx cd, CCT ${cct}00K, CRI>${cri}0</span>`

        //Warning 
        document.getElementById('Warning').innerHTML=e_value[0]?`${e_value[0].Warning}`+`${e_value[0].Warning2}`:`<span class="error">N/A</span>`
        document.getElementById('Warning_card').innerHTML=e_value[0]?`${e_value[0].Warning}`+`<span class='KJ_Fonts_Warning'>${e_value[0].Warning2}</span>`:`<span class="error">N/A</span>`

        //Certification
        document.getElementById('Certification').src=e_value[0]?`${e_value[0].Certification}`:`<span class="error">N/A</span>`
        document.getElementById('cert').innerHTML=e_value[0]?`<img src=${e_value[0].Certification2}></img>`:``

        //Box Label image
        document.getElementById('sec_2').src=e_value[0]?`${e_value[0].Box_label_img}`:`<span class="error">N/A</span>`


        //Mounting Box
        if (mo==='X39'){
            document.getElementById('Mounting_box').innerHTML=`<input class="input_text" id="Socket_Box_input" type="text" style="width: 50px; height: 12px; padding:0" value="EX39"></input>`
        }else{
            document.getElementById('Mounting_box').innerHTML=`<input class="input_text" id="Socket_Box_input" type="text" style="width: 50px; height: 12px; padding:0" value="${mo}"></input>`
        }
       

        window.onerror = function(e){
        document.getElementById("error").innerHTML = 'No Value, please select again'
        }
        

    }

 //Enter submit
 const Submit_Enter= async () => {
    const temp = document.getElementById('Enter_PartNumber').value;
    const arr = temp.trim().split('-')
    lu = await lumen;
    In = await Info;
    ra = await raw;
    if (arr.length===11){

        const fa= arr[0]
        const mo= arr[1]
        const po= arr[2]
        const cri=arr[3]
        const cct=arr[4]
        const dri=arr[5]
        document.getElementById('Part').innerHTML=`${fa}-${mo}-${po}-${cri}-${cct}-${dri}-AN-B90-000-000-FL`
        document.getElementById('PartNumber_box').innerHTML=`<input class="input_text" id="PartNumber_Box_input" size=40 type="text" style="height: 15px" value="${fa}-${mo}-${po}-${cri}-${cct}-${dri}-AN-B90-000-000-FL"></input>`;
        document.getElementById('PartNumber_card').innerHTML=`<input class="input_text" id="PartNumber_input" type="text" size=30 style="height: 12px" value="${fa}-${mo}-${po}-${cri}-${cct}-${dri}-AN-B90-000-000-FL"></input>`;

        //Title & Product code
        const code = ra.filter((item)=>{
            const temp=document.getElementById('Part').innerHTML
            console.log(temp)
            return item&&item.PartNumber===temp
        })

        document.getElementById('Title').innerHTML=code[0]?`${code[0].Title.toUpperCase()}`:`<span class="error">N/A</span>`;
        document.getElementById('box_title').innerHTML=code[0]?`<input class="input_text" id="box_title_input" type="text" value="${code[0].Title.toUpperCase()}"></input>`:`<span class="error">N/A</span>`;
        document.getElementById('Title_card').innerHTML=code[0]?`<input class="input_text" id="Title_card_input" style="width: 100px; height: 12px; padding:0" type="text" value="${code[0].Title.toUpperCase()}"></input> <strong>(Modelo,  <span>모델, モデル</span>)</strong>`:`<span class="error">N/A</span>`

        

        const zeroPad = (num, places) => String(num).padStart(places, '0')

        document.getElementById('Code').innerHTML=code[0]?`${zeroPad(code[0].Code, 5)}`:`<span class="error">N/A</span>`
        document.getElementById('Code_box').innerHTML=code[0]?`<input class="input_text" id="Code_box_input" type="text" size=10 style="height: 15px" value="${zeroPad(code[0].Code, 5)}"></input>`:`<span class="error">N/A</span>`
        document.getElementById('PartNumber_card').innerHTML+=code[0]?`<input class="input_text" id="PartNumber_card_input" type="text" style="width: 32px; height: 12px; padding:0" value="${zeroPad(code[0].Code, 5)}"></input>`:``

        //Barcode
        console.log((code[0].Barcode))
        document.getElementById('footer').innerHTML=`<svg class="barcode" id='barcode'
        jsbarcode-format="upc"
        jsbarcode-value=${code[0].Barcode}
        jsbarcode-textmargin="0"
        jsbarcode-fontoptions="bold">
        </svg>`
        JsBarcode(".barcode").init();

        const e_value = In.filter((item)=>{
            return item&&item.Family===fa&&item.Mounting===mo&&item.Power===parseInt(po)&&item.Driver===dri

        })

        document.getElementById('Electrical').innerHTML= e_value[0]?`${e_value[0].Electrical_Input}`:`<span class="error">N/A</span>`
        document.getElementById('Electrical_box').innerHTML= e_value[0]?`<input class="input_text" id="Electrical_box_input" size=35 type="text" style="height: 15px; padding:0" value="${e_value[0].Electrical_Input}"></input>`:`<span class="error">N/A</span>`
        document.getElementById('Electrical_card').innerHTML= e_value[0]?`<input class="input_text" id="Electrical_card_input" type="text" style="width: 150px; height: 10px; padding:0" value="${e_value[0].Electrical_Input}"></input>`:`<span class="error">N/A</span>`
        
       //Optical Output
        const lu_value = lu.filter((item)=>{
            return item.Family===fa&&item.Power===parseInt(po)&&item.CCT===parseInt(cct)&&item.CRI.toString()===cri.toString()
        })
        document.getElementById('Optical').innerHTML=lu_value[0]? `${lu_value[0].Lumen} Lumens, xxx cd, CCT ${cct}00K, CRI>${cri}0`:`<span class="error">N/A Lumens, xxx cd, CCT ${cct}00K, CRI>${cri}0</span>`
        document.getElementById('Optical_box').innerHTML=lu_value[0]? `<input class="input_text" id="Optical_box_input" type="text" style="width: 180px; height: 12px; padding:0" value="${lu_value[0].Lumen} Lumens, xxx cd, CCT ${cct}00K, CRI>${cri}0"></input>`:`<input class="input_text" id="Optical_card_input" type="text" style="width: 160px; height: 12px; padding:0" value="N/A Lumens, xxx cd, CCT ${cct}00K, CRI>${cri}0"></input>`
        document.getElementById('Optical_card').innerHTML=lu_value[0]? `<input class="input_text" id="Optical_card_input" type="text" style="width: 160px; height: 12px; padding:0" value="${lu_value[0].Lumen} Lumens, xxx cd, CCT ${cct}00K, CRI>${cri}0"></input>`:`<input class="input_text" id="Optical_card_input" type="text" style="width: 160px; height: 12px; padding:0" value="N/A Lumens, xxx cd, CCT ${cct}00K, CRI>${cri}0"></input>`

        //Warning 
        document.getElementById('Warning').innerHTML=e_value[0]?`${e_value[0].Warning}`+`${e_value[0].Warning2}`:`<span class="error">N/A</span>`
        document.getElementById('Warning_card').innerHTML=e_value[0]?`${e_value[0].Warning}`+`<span class='KJ_Fonts_Warning'>${e_value[0].Warning2}</span>`:`<span class="error">N/A</span>`

        //Certification
        document.getElementById('Certification').src=e_value[0]?`${e_value[0].Certification}`:`<span class="error">N/A</span>`
        document.getElementById('cert').innerHTML=e_value[0]?`<img src=${e_value[0].Certification2}></img>`:``

        //Box Label image
        document.getElementById('sec_2').src=e_value[0]?`${e_value[0].Box_label_img}`:`<span class="error">N/A</span>`


        //Mounting Box
        if (mo==='X39'){
            document.getElementById('Mounting_box').innerHTML=`<input class="input_text" id="Socket_Box_input" type="text" style="width: 50px; height: 12px; padding:0" value="EX39"></input>`
        }else{
            document.getElementById('Mounting_box').innerHTML=`<input class="input_text" id="Socket_Box_input" type="text" style="width: 50px; height: 12px; padding:0" value="${mo}"></input>`
        }
       

        window.onerror = function(e){
        document.getElementById("error").innerHTML = 'Missing value'
        }
        




    }else{
        document.getElementById('error').innerHTML='PartNumber is not valid!'
        setTimeout(function(){ document.getElementById('error').innerHTML="" }, 5000);
    }
 } 

 
 //Print Product Label
 function Refresh(){
    document.getElementById('Title_card1').innerHTML =true ? `${document.getElementById('Title_card_input').value} <strong>(Modelo,  <span>모델, モデル</span>)</strong>`:`<span class="error">N/A</span>`;
    document.getElementById('PartNumber_card1').innerHTML= true?document.getElementById('PartNumber_input').value + ` (${document.getElementById('PartNumber_card_input').value})`:`<span class="error">N/A</span>`;
    document.getElementById('Electrical_card1').innerHTML=true?document.getElementById('Electrical_card_input').value:`<span class="error">N/A</span>`;
    document.getElementById('Optical_card1').innerHTML = true?`${document.getElementById('Optical_card_input').value}`:`<span class="error">N/A</span>`;
    document.getElementById('Warning_card1').innerHTML= true?document.getElementById('Warning_card').innerHTML:`<span class="error">N/A</span>`;
    document.getElementById('cert1').innerHTML = true?document.getElementById('cert').innerHTML:`<span class="error">N/A</span>`;
 }

 //Print Box Label
 function Refresh1(){
    document.getElementById('box_title_1').innerHTML =true?`${document.getElementById('box_title_input').value}`:`<span class="error">N/A</span>`;
    document.getElementById('PartNumber_box1').innerHTML= true?`${document.getElementById('PartNumber_Box_input').value}`:`<span class="error">N/A</span>`;
    document.getElementById('Code_box1').innerHTML= true?`${document.getElementById('Code_box_input').value}`:`<span class="error">N/A</span>`;
    document.getElementById('Electrical_box1').innerHTML=true?document.getElementById('Electrical_box_input').value:`<span class="error">N/A</span>`;
    document.getElementById('Optical_box1').innerHTML = true?`${document.getElementById('Optical_card_input').value}`:`<span class="error">N/A</span>`;
    document.getElementById('Mounting_box1').innerHTML = true?`${document.getElementById('Socket_Box_input').value}`:`<span class="error">N/A</span>`;
    document.getElementById('sec_3').src = true?document.getElementById('sec_2').src:``;
    document.getElementById('footer1').innerHTML=true?document.getElementById('footer').innerHTML:`<span class="error">N/A</span>`;

    //Korean box label
    document.getElementById('box_title_2').innerHTML =true?`${document.getElementById('box_title_input').value}`:`<span class="error">N/A</span>`;
    document.getElementById('PartNumber_box2').innerHTML= true?`${document.getElementById('PartNumber_Box_input').value}`:`<span class="error">N/A</span>`;
    document.getElementById('Code_box2').innerHTML= true?`${document.getElementById('Code_box_input').value}`:`<span class="error">N/A</span>`;
    document.getElementById('Electrical_box2').innerHTML=true?document.getElementById('Electrical_box_input').value:`<span class="error">N/A</span>`;
    document.getElementById('Optical_box2').innerHTML = true?`${document.getElementById('Optical_card_input').value}`:`<span class="error">N/A</span>`;
    document.getElementById('Mounting_box2').innerHTML = true?`${document.getElementById('Socket_Box_input').value}`:`<span class="error">N/A</span>`;
    document.getElementById('sec_4').src = true?document.getElementById('sec_2').src:``;
    document.getElementById('footer2').innerHTML=true?`${document.getElementById('footer').innerHTML}
    <div class="Korea_supplier">
        공급처 ㈜세미백아이엔씨 써비스센터 연락처제조자:<br>
        불산 HuaQuan 전기 조명 CO., LTD. <br>
        HU11458-18001A (안전인증번호) (+82) 031-591-318 <br>
    </div>`:`<span class="error">N/A</span>`
}
